import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    first: 0,
    second: 0,
    third: 0
  },
  mutations: {
    setFirst(state, value) {
      state.first = value;
    },
    setSecondAndThird(state, value) {
      state.second = value;
      state.third = value;
    }
  }
})
