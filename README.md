# Тестовое задание для ПАРИТЕТ РДЦ

[Demo](https://paritet-test-job.web.app/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
